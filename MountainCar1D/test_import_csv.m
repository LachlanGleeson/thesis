files = dir('MC_fa/*.csv');
n = length(files);

data = cell(1,n);
for i=1:n
    hold on
    data{i} = csvread(strcat('MC_fa/', files(i).name));
    plot3(data{i}(:,1), data{i}(:,2), data{i}(:,3), 'o-')
    view(3);
end

hold off
