#!/bin/python3.5
import gym
import numpy as np
from Transition import Transition as T
import random

env = gym.make("MountainCar-v0")
bins = 15
steps = 5
episodes = 50
debug = False

#(env, bins, steps, verbose)
transition = T(env, bins, steps, episodes, debug)


print(str(transition))

for a in range(env.action_space.n):
    for s in range(transition.S_size):
        p_sum = 0
        ep, st = transition.expand_state(s)
        test_s = transition.flatten_state(ep, st)
        if test_s != s:
            raise Exception("bad test " + str(ep) + str(st) 
                    + "expanded to " + str(test_s) + "not " + str(s))
            
        for _s, p in enumerate(transition.matrix[a][s]):
            if p > 0:
                p_sum += p
        if p_sum != 1.0:
            print("s: %d, a: %d ==> _s: %d" % (s, a, _s))
            step, state = transition.expand_state(s)
            _step, _state = transition.expand_state(_s)
            print("state transition:\n\tstep: %d -> _step: %d" % (step, 
                    _step) + "\n\tstate: (%.4f, %.4f) ->" % (state[0],
                    state[1]) + " _state: (%.4f, %.4f)" % (_state[0], 
                    _state[1]))
            print("T[%d][%d][%d] = " % (a, s, _s) +
                    str(transition.matrix[a][s][_s]))
            raise Exception("p_sum Exception")
