clear all;
A = importdata('RANK_SUMMARY.csv');
c = categorical(A.colheaders);
f = figure();
%f.set('TickLabelInterpreter', 'none');
hold on;
bar(c, A.data(1,:), 'EdgeColor', 'black', 'FaceColor', 'white');
errorbar(c, A.data(1, :), A.data(2,:), '.k');
ylabel('Average score (1000 simulations)')
xlabel('Policy');
hold off;