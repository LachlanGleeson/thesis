import gym
import numpy as np

from Reward_mixing_reward import Reward_mixing_reward as R
from Transition import Transition
import sys
import getopt

from Reward_true_reward import Reward_true_reward as Reward
from Reward_movement_cost import Reward_movement_cost as rMoveCost
from Reward_action_cost import Reward_action_cost as rActionCost
from Reward_height_bonus import Reward_height_bonus as rHeightBonus
from Reward_straight_line import Reward_straight_line as rStraightLine
from Reward_feed_accel import Reward_feed_accel as rFeedAccel
import mdptoolbox


d = 0.99
N = 1000000
bins = 0
eps=0.0000001
verbose = False
episodes = 0
steps = 0
file_prefix = 'MC_mix'

def useage(argv):
    print("Python 3.5.4 script to run MDP toolbox on OpenAI gym classic")
    print("control environments\n\nusage:")
    print(argv[0] + " [--output {filename}] [--discout {d}] [--max-iter {i}] || --help")


def parse_args(argv):
    global verbose, d, N, bins, filename, eps
    try:
        opts, args = getopt.getopt(argv[1:], "vhb:d:N:p:e:", ["verbose", "help",
        "bins=", "discount=", "max-iter=", "prefix=", "epsilon="])
    except getopt.GetoptError:
        useage(argv)
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            useage(argv)
            sys.exit(0)
        elif o in ("-d", "--disount"):
            d = a
        elif o in ("-N", "--max-iter"):
            N = a
        elif o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-b", "--bins"):
            bins = a
        elif o in ("-p", "--prefix"):
            filename = a
        elif o in ("-e", "--epsilon"):
            eps = a


def load_params():
    global bins, episodes, steps
    try:
        param_file = open('MDP.params', 'r')
        for line in param_file.readlines():
            if "bins" in line:
                bins = int(line.replace("bins = ", ""))
            elif "episodes" in line:
                episodes = int(line.replace("episodes = ", ""))
            elif "steps" in line:
                steps = int(line.replace("steps = ", ""))
    except Exception as e:
        print("faield to get param file!")
        print(e)
    print("loaded params %d bins %d steps" % (bins, episodes))


def solve_MDP(transition, reward):
    MDP_vi = mdptoolbox.mdp.ValueIteration(transition.matrix, reward.matrix, 
            d, eps, N)
    print("MDP loaded sucessfully!")
    if verbose:
       MDP_vi.setVerbose()

    MDP_vi.run()
    print("MDP_vi complete!")
    if verbose:
        print("T:" + str(transition) + " R:" + str(reward))
        print(MDP_vi.policy)
        print("policy len = " + str(len(MDP_vi.policy)))
        print("solved in " + MDP_vi.iter + " iterations")
    return MDP_vi.policy


def calc_std_err(average, scoreList):
    scoreList = np.array(scoreList)
    square_diff = (scoreList - average)**2
    stdev = np.sqrt(np.sum(square_diff)/len(scoreList))
    return stdev / np.sqrt(len(scoreList))


def time_step(env, steps, action):
    state = None
    score = 0
    done = False
    for step in range(steps):
        state, r, done, _ = env.step(action)
        score += r
        if done:
            break
    return state, score, done


def run(argv):
    parse_args(argv)
    load_params()
    env = gym.make("MountainCar-v0")

    transition = Transition(env, bins, steps, episodes, verbose)
    reward = r_true_reward = Reward(env, bins, steps, episodes, verbose)
    a, s, _s = np.shape(reward.matrix)
    print("MDP space:\na = " + str(a) + " s = " + str(s) + " _s = " + str(_s))
    r_move_cost = rMoveCost(env, bins, steps, episodes, verbose)
    r_action_cost = rActionCost(env, bins, steps, episodes, verbose)
    r_height_bonus = rHeightBonus(env, bins, steps, episodes, verbose)
    r_straight_line = rStraightLine(env, bins, steps, episodes, verbose)
    r_feed_accel = rFeedAccel(env, bins, steps, episodes, verbose)
    reward_list = [r_true_reward, r_move_cost, r_action_cost, r_feed_accel, 
            r_height_bonus, r_straight_line]
    for reward_a in reward_list:
        for reward_b in reward_list:
            if reward_a == reward_b:
                continue
            reward_summary = []
            for x in range(11):
                prop_a = x * 0.1
                prop_b = 1 - prop_a
                prop = (prop_a, prop_b)
                mixed_reward = R(env, bins, steps, episodes, reward_a, reward_b,
                        prop, verbose)
                policy = solve_MDP(transition, mixed_reward)
                mix_name = "%.2f * %s + %.2f * %s" % (prop_a, 
                        str(reward_a.suffix()), prop_b, str(reward_b.suffix()))
                high_score_string = policy_string = "Policy: " + mix_name
                    ##use reward policy here##
                high_score = -(episodes * steps)
                total_score = 0
                score_log = []
                num_sims = 1000
                for sim in range(num_sims):
                    state = env.reset()
                    score = 0
                    episode = 0
                    while episode < episodes:
                        flatState = mixed_reward.flatten_state(episode, state)
                        action = policy[flatState]
                        state, r, done = time_step(env, steps, action)
                        score += r
                        if done:
                            break
                        episode += 1
                    if score > high_score:
                        high_score = score
                        high_score_string = policy_string
                    total_score += score
                    score_log.append(score)
                average_score = total_score / num_sims
                std_err = calc_std_err(average_score, score_log)
#                print("high score mix:\n" + high_score_string)
#                print("max score = %d, average score = %.4f" % (high_score,
#                        average_score))
                reward_summary.append({'name': mix_name, 'high_score':
                        high_score, 'avg_score': average_score, 'std_err':
                        std_err})
            print(str(reward_summary))
            prefix = reward_a.suffix() + "_" + reward_b.suffix()
            avg_summary = []
            stderr_summary = []
            higscr_summary = []
            for summary in reward_summary:
                avg_summary.append(summary.get('avg_score'))
                higscr_summary.append(summary.get('high_score'))
                stderr_summary.append(summary.get('std_err'))
            summary_out = np.vstack([higscr_summary, avg_summary,
                    stderr_summary])
            np.savetxt( "MIX/MIX_" + prefix + ".csv" , summary_out,
                    delimiter=',', fmt='%.5f')


    print("Writing run parameters to MDP.params")
    try:
        file_object = open('MDP_mix.params', 'w')
        file_object.write("bins = %d\n" % bins)
        file_object.write("episodes = %d\n" % episodes)
        file_object.close()
    except Exception as e:
        print("file write failed!")
        print(e)
        


if __name__ == '__main__':
    run(sys.argv)

