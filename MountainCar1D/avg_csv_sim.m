clear all;
basedir = '/home/lg/Thesis/myrepo/MountainCar1D/';
dirPattern = 'MC_*';
files = dir(strcat(basedir, dirPattern));
dirFlags = [files.isdir];
%extract dirs from files, dirs = csv dirs
csvDirs = files(dirFlags);
averagePath = cell(1:length(csvDirs));
for j = 1:length(csvDirs)
    csvFiles = dir(strcat(basedir, csvDirs(j).name, '/*.csv'));
    n = length(csvFiles);
    data = cell(1, n);
    for i = 1:n
        data{i} = csvread(strcat(csvFiles(i).folder, '/', csvFiles(i).name));
    end

    lowerSum = zeros(20,3);
    lowX = [];
    lowY = [];
    upperSum = zeros(29, 3);
    upperX = [];
    upperY = [];
    %lc = 0;
    %uc = 0;
    for k = 1:length(data)
        if (length(data{k}(:,1)) < 28) && (length(data{k}(:,1)) >= 20)
            %lc = lc + 1;
            lowX = [lowX, data{k}(1:20, 1)];
            lowY = [lowY, data{k}(1:20, 2)];
            lowZ = data{k}(1:20, 3);
            %for m = 1:20
            %    lowerSum(m,:) = lowerSum(m,:) + data{k}(m,:);
            %end
        end
        if (length(data{k}(:,1)) >= 29) && (length(data{k}(:,1)) < 35)
            upperX = [upperX, data{k}(1:29, 1)];
            upperY = [upperY, data{k}(1:29, 2)];
            upperZ = data{k}(1:29, 3);
            %uc = uc + 1;
            %for m = 1:29
            %    upperSum(m,:) = upperSum(m,:) + data{k}(m,:);
            %end
        end
    end
    
    figure()
    hold on;
    view(3);
    %average first solutions
    n = size(lowX, 2);
    if n > 3    
        Xavg = sum(lowX, 2) / n;
        Yavg = sum(lowY, 2) / n;
        Zavg = lowZ;
        plot3(Xavg, Yavg, Zavg, '.-k')
        Xdiff = (lowX - Xavg).^2;
        sdX = sqrt(sum(Xdiff, 2) / n);
        seX = sdX / sqrt(n);
        Ydiff = (lowY - Yavg).^2;
        sdY = sqrt(sum(Ydiff, 2) / n);
        seY = sdY / sqrt(n);
        xV = [Xavg, Xavg];
        yV = [Yavg, Yavg];
        zV = [Zavg, Zavg];
        xMin = Xavg + seX;
        xMax = Xavg - seX;
        yMin = Yavg + seY;
        yMax = Yavg - seY;
        xB = [xMin, xMax];
        yB = [yMin, yMax];
        for e = 1:length(xB)
            plot3(xB(e, :), yV(e, :), zV(e, :), '-k')
            plot3(xV(e, :), yB(e, :), zV(e, :), '-k')
        end
    end
    %average second solutions
    n = size(upperX, 2);
    if n > 3
        Xavg = sum(upperX, 2) / n;
        Yavg = sum(upperY, 2) / n;
        Zavg = upperZ;
        plot3(Xavg, Yavg, Zavg, '.-k')
        Xdiff = (upperX - Xavg).^2;
        sdX = sqrt(sum(Xdiff, 2) / n);
        seX = sdX / sqrt(n);
        Ydiff = (upperY - Yavg).^2;
        sdY = sqrt(sum(Ydiff, 2) / n);
        seY = sdY / sqrt(n);
        xV = [Xavg, Xavg];
        yV = [Yavg, Yavg];
        zV = [Zavg, Zavg];
        xMin = Xavg + seX;
        xMax = Xavg - seX;
        yMin = Yavg + seY;
        yMax = Yavg - seY;
        xB = [xMin, xMax];
        yB = [yMin, yMax];
        for e = 1:length(xB)
            plot3(xB(e, :), yV(e, :), zV(e, :), '-k')
            plot3(xV(e, :), yB(e, :), zV(e, :), '-k')
        end
    end
    title(csvDirs(j).name, 'Interpreter', 'none');
    xlabel('Position [units]')
    ylabel('Velocity [units/time step]')
    zlabel('Time step')
end    