clear all;
basedir = '/home/lg/Thesis/myrepo/MountainCar2D/MIX/';
dirPattern = 'MIX_*.csv';
csvFiles = dir(strcat(basedir, dirPattern));

for j = 1:length(csvFiles)
    f = figure('Visible', 'off');
    hold on;
    data = csvread(strcat(basedir, csvFiles(j).name));
    bar(0:0.1:1, data(2,:), 'EdgeColor', 'black', 'FaceColor', 'white');
    scatter(0:0.1:1, data(1,:), 'xk');
    errorbar(0:0.1:1, data(2, :), data(3,:) * sqrt(1000), '.k');
    name = strrep(strrep(csvFiles(j).name, 'MIX_', ' '), '.csv', '');
    xlabel(strcat('Mix of ', name), 'Interpreter', 'none')
    ylabel('Average score (1000 simulations)')
    %title(strcat('Mixing reward: ', csvFiles(j).name), 'Interpreter', 'none')
    hold off 
    saveas(f, strcat('MIX/', strrep(csvFiles(j).name, '.csv', '.svg')), 'svg')
    close(f);
end