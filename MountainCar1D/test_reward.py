#!/bin/python3.5
import gym
import numpy as np
from Reward_true_reward import Reward_true_reward as R
import random

env = gym.make("MountainCar-v0")

#(env, bins, steps, verbose)
reward = R(env, 20, 50, True)

action_sample = random.sample(range(0, 3), 2)
state_sample = random.sample(range(1, reward.S_size), 100)

print(str(reward))

for a in action_sample:
    for s in state_sample:
        for _s, r in enumerate(reward.matrix[a][s]):
            if r > 0:
                print("s: %d, a: %d ==> _s: %d" % (s, a, _s))
                step, state = reward.expand_state(s)
                _step, _state = reward.expand_state(_s)
                print("%d:(%.4f, %.4f) ==> %d:(%.4f, %.4f)" % (step, state[0],
                        state[1], _step, _state[0], _state[1]))
                print("R[%d][%d][%d] = " % (a, s, _s) +
                        str(reward.matrix[a][s][_s]))
