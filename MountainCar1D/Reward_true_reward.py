#!/bin/python3.5

import gym
import numpy as np
from Reward import Reward

class Reward_true_reward(Reward):


    def __init__(self, env = "MountainCar-v0", n_bins = 50, steps = 10,
            episodes = 20, debug = True):
        super(Reward_true_reward, self).__init__(env, n_bins, steps, episodes, 
                debug)
        self.build_reward_matrix()


    def set_reward(self, action, stateEpisode):
        ep, s = self.expand_state(stateEpisode)
        _s = self.map_state_action_nstate(s, action)
        _stateEpisode = np.clip(self.flatten_state(ep + 1, _s), 0, 
                self.S_size - 1)
        if self.height(_s) >= self.height((self.env.env.goal_position, 0)):
            self.matrix[action][stateEpisode][_stateEpisode] = 0
            if self.debug:
                print("ep" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:1")
        else:
            self.matrix[action][stateEpisode][_stateEpisode] = -1
            if self.debug:
                print("ep:" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:0")


    def suffix(self):
        return "tr"
