import numpy as np
import gym
import math


class StateMap(object):

    #env = openai gym env
    #bins = (int) number of division in pos/vel
    #steps = (int) simulation length (# of actions)
    def __init__(self, env, n_bins, steps, sim_length):
        if env == None or n_bins < 1 or steps < 1 or sim_length < 1:
            raise InvalidArgumentException("Invalid argument detected")
        self.env = env
        self.n_bins = n_bins
        self.steps = steps
        self.n_episodes = sim_length
        self.n_states = self.n_bins * self.n_bins + self.n_bins
        self.S_size = self.n_episodes * self.n_states + self.n_states


    #return height above 0
    def height(self, s):
        return self.env.env._height(s[0])


    #return the force of gravity acting on the car at
    #the position specified by s[0] (pos)
    def g_force(self, s):
        return (-0.0025) * math.cos(3 * s[0])


    #state = tuple of (pos, v) action = 0, 1 or 2
    #returns s'
    def map_state_action_nstate(self, s, a):
        self.env.reset()
        self.env.env.state=s
        state = None
        #perform same action
        for i in range(self.steps):
            state, reward, done, msg = self.env.step(a)
        return state


    #return the {0, ..., self.n_bins} that the state components
    #are in
    def state_bins(self, s):
        env_low = np.array(self.env.observation_space.low, dtype='float64')
        env_high = np.array(self.env.observation_space.high, dtype='float64')
        env_dx = (env_high - env_low) / self.n_bins
        a = np.clip(int((s[0] - env_low[0])/env_dx[0]), 0, self.n_bins)
        b = np.clip(int((s[1] - env_low[1])/env_dx[1]), 0, self.n_bins)
        return a, b


    #convert state from (pos, v) tuple to linear representaion
    def flatten_state(self, episode, s, posError = 0, vError = 0):
        env_low = np.array(self.env.observation_space.low, dtype='float64')
        env_high = np.array(self.env.observation_space.high, dtype='float64')
        env_dx = (env_high - env_low) / self.n_bins


        p_bin = np.clip(int(np.around((s[0] - env_low[0])/env_dx[0], 6) +
                posError), 0, self.n_bins)
        v_bin = np.clip(int(np.around((s[1] - env_low[1])/env_dx[1], 6) +
                vError), 0, self.n_bins)
        _state = p_bin * self.n_bins + v_bin
        return episode * self.n_states + _state


    #convert state from linear representation to (pos, v) tuple
    def expand_state(self, flatState):
        _state = flatState % self.n_states
        episode = int(flatState / self.n_states)

        _pos = int(_state / self.n_bins)
        _v = _state % self.n_bins

        env_low = np.array(self.env.observation_space.low, dtype='float64')
        env_high = np.array(self.env.observation_space.high, dtype='float64')
        env_dx = (env_high - env_low) / self.n_bins
        
        pos = _pos * env_dx[0] + env_low[0]
        v = _v * env_dx[1] + env_low[1]
        state = (pos, v)
        return episode, state
