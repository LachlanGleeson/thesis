#!/bin/python3.5

import sys
import getopt

from Transition import Transition
from Reward_true_reward import Reward_true_reward as Reward
from Reward_movement_cost import Reward_movement_cost as rMoveCost
from Reward_action_cost import Reward_action_cost as rActionCost
from Reward_height_bonus import Reward_height_bonus as rHeightBonus
from Reward_straight_line import Reward_straight_line as rStraightLine
from Reward_sharp_turn import Reward_sharp_turn as rSharpTurn
from Reward_feed_accel import Reward_feed_accel as rFeedAccel
import mdptoolbox
import numpy as np
import gym


d = 0.99
N = 1000000
bins = 20
eps=0.0000001
verbose = False
steps = 5
episodes = 39
file_prefix = 'MC_'

def useage(argv):
    print("Python 3.5.4 script to run MDP toolbox on OpenAI gym classic")
    print("control environments\n\nusage:")
    print(argv[0] + " [--output {filename}] [--discout {d}] [--max-iter {i}] || --help")


def parse_args(argv):
    global verbose, d, N, bins, filename, eps
    try:
        opts, args = getopt.getopt(argv[1:], "vhb:d:N:p:e:", ["verbose", "help",
        "bins=", "discount=", "max-iter=", "prefix=", "epsilon="])
    except getopt.GetoptError:
        useage(argv)
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            useage(argv)
            sys.exit(0)
        elif o in ("-d", "--disount"):
            d = a
        elif o in ("-N", "--max-iter"):
            N = a
        elif o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-b", "--bins"):
            bins = a
        elif o in ("-p", "--prefix"):
            filename = a
        elif o in ("-e", "--epsilon"):
            eps = a


def solve_MDP(transition, reward):
    MDP_vi = mdptoolbox.mdp.ValueIteration(transition.matrix, reward.matrix, 
            d, eps, N)
    print("MDP loaded sucessfully!")
    if verbose:
       MDP_vi.setVerbose()

    MDP_vi.run()
    print("MDP_vi complete!")
    if verbose:
        print("T:" + str(transition) + " R:" + str(reward))
        print(MDP_vi.policy)
        print("policy len = " + str(len(MDP_vi.policy)))
        print("solved in " + MDP_vi.iter + " iterations")
    np.savetxt( file_prefix + reward.suffix() + ".policy", MDP_vi.policy)


def run(argv):
    parse_args(argv)

    env = gym.make("MountainCar-v0")

    transition = Transition(env, bins, steps, episodes, verbose)
    reward = r_true_reward = Reward(env, bins, steps, episodes, verbose)
    a, s, _s = np.shape(reward.matrix)
    print("MDP space:\na = " + str(a) + " s = " + str(s) + " _s = " + str(_s))
    r_move_cost = rMoveCost(env, bins, steps, episodes, verbose)
    r_action_cost = rActionCost(env, bins, steps, episodes, verbose)
    r_height_bonus = rHeightBonus(env, bins, steps, episodes, verbose)
    r_straight_line = rStraightLine(env, bins, steps, episodes, verbose)
    r_sharp_turn = rSharpTurn(env, bins, steps, episodes, verbose)
    r_feed_accel = rFeedAccel(env, bins, steps, episodes, verbose)
    reward_list = [r_true_reward, r_move_cost, r_action_cost, r_feed_accel,
            r_height_bonus, r_straight_line, r_sharp_turn]
    for r in reward_list:
        solve_MDP(transition, r)
        ##use reward policy here##
    print("Writing run parameters to MDP.params")
    try:
        file_object = open('MDP.params', 'w')
        file_object.write("bins = %d\n" % bins)
        file_object.write("episodes = %d\n" % episodes)
        file_object.write("steps = %d\n" % steps)
        file_object.close()
    except Exception as e:
        print("file write failed!")
        print(e)
        


if __name__ == '__main__':
    run(sys.argv)
