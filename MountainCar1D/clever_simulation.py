#!/bin/python3.5

import glob, time
import os
import sys
import gym
import numpy as np
#from Reward import Reward as R
from StateMapper import StateMap as SM

verbose = True
environment = "MountainCar-v0"
bins = 0
episodes = 0
steps = 0

def load_policies():
    policies = []
    for fileName in glob.glob('*.policy'):
        print(str(fileName))
        matrix = np.loadtxt(fileName)
        matrix = matrix.astype(int)
        print("file " + str(fileName) + " contains:" + str(matrix))
        print("matrix.dimensions = " + str(matrix.shape))
        policy = {'name': str(fileName), 'policy': matrix}
        policies.append(policy)
    return policies


def time_step(env, stateMap, steps, episode, policy):
    score = 0
    done = False
    for step in range(steps):
        epState = stateMap.flatten_state(episode, env.env.state)
#        print(str(epState) + str(env.env.state))
        action = policy[epState]
        state, r, done, _ = env.step(action)
        score += r
        if done:
            break
    return score, done


def run(argv):
    global bins, episodes, verbose, steps
    policies = load_policies()
    env = gym.make(environment)
    try:
        param_file = open('MDP.params', 'r')
        for line in param_file.readlines():
            if "bins" in line:
                bins = int(line.replace("bins = ", ""))
            elif "episodes" in line:
                episodes = int(line.replace("episodes = ", ""))
            elif "steps" in line:
                steps = int(line.replace("steps = ", ""))

    except Exception as e:
        print("faield to get param file!")
        print(e)

    print("n_bins: " + str(bins))
    print("n_eps: " + str(episodes))
    print("n_steps: " + str(steps))
    state_map = SM(env, bins, steps, episodes)
    for p in policies:
        policy = p['policy']
        name = p['name']
        high_score = -(episodes * steps)
        average_score = 0
        num_sims = 10000
        for sim in range(num_sims):
            state = env.reset()
            score = 0
            done = False
            for episode in range(episodes):
                r, done = time_step(env, state_map, steps, episode, policy)
                score += r
                if done:
                    break
            average_score += score
            if score > high_score:
                print("sim %d new high score!" % sim)
                high_score = score
        print("policy: %s high score = %d, average score = %d" % (name, 
                high_score, int(average_score / num_sims)))


if __name__ == '__main__':
   sys.exit(run(sys.argv))
