clear all;
basedir = '/home/lg/Thesis/myrepo/MountainCar2D/';
dirPattern = 'MC3D_*';
files = dir(strcat(basedir, dirPattern));
dirFlags = [files.isdir];
%extract dirs from files, dirs = csv dirs
csvDirs = files(dirFlags);
for j = 1:length(csvDirs)
   csvFiles = dir(strcat(basedir, csvDirs(j).name, '/*.csv'));
   n = length(csvFiles);
   data = cell(1, n);
   figure();
   hold on;
   for i=1:n
       data{i} = csvread(strcat(csvFiles(i).folder, '/', csvFiles(i).name));
       plot3(data{i}(:,1), data{i}(:,2), data{i}(:,5), '.-')
   end
   view(3);
   title(csvDirs(j).name, 'Interpreter', 'none');
   xlabel('X position [units]')
   ylabel('Y position [units]')
   zlabel('Time step')
   hold off
end