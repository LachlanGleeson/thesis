#!/bin/python3.5

import glob, time
import os
import sys
import gym
import numpy as np
from Reward import Reward as R

verbose = True
environment = "MountainCar-v0"
bins = 0
episodes = 0
steps = 0

def load_policies():
    policies = []
    for fileName in glob.glob('MC_*.policy'):
        print(str(fileName))
        matrix = np.loadtxt(fileName)
        matrix = matrix.astype(int)
        print("file " + str(fileName) + " contains:" + str(matrix))
        print("matrix.dimensions = " + str(matrix.shape))
        policy = {'name': str(fileName), 'policy': matrix}
        policies.append(policy)
    return policies


def calculate_stats(summedScores, numSims, score_list):
    average = summedScores / numSims
    scoreList = np.array(score_list)
    square_diff = (scoreList - average)**2
    stdev = np.sqrt(np.sum(square_diff)/len(scoreList))
    return average, stdev



def time_step(env, steps, action):
    state = None
    score = 0
    done = False
    for step in range(steps):
        state, r, done, _ = env.step(action)
        score += r
        if done:
            break
    return state, score, done


def run(argv):
    global bins, episodes, verbose, steps
    policies = load_policies()
    env = gym.make(environment)
    try:
        param_file = open('MDP.params', 'r')
        for line in param_file.readlines():
            if "bins" in line:
                bins = int(line.replace("bins = ", ""))
            elif "episodes" in line:
                episodes = int(line.replace("episodes = ", ""))
            elif "steps" in line:
                steps = int(line.replace("steps = ", ""))

    except Exception as e:
        print("faield to get param file!")
        print(e)

    print("n_bins: " + str(bins))
    print("n_eps: " + str(episodes))
    print("n_steps: " + str(steps))
    reward = R(env, bins, steps, episodes, verbose)
    summary = np.zeros(shape=(3, len(policies)))
    header_string = ''
    for count, p in enumerate(policies):
        policy = p['policy']
        name = p['name']
        high_score = -(episodes * steps)
        summedScore = 0
        num_sims = 1000
        score_list = []
        for sim in range(num_sims):
            state = env.reset()
            score = 0
            done = False
            for episode in range(episodes):
                flatState = reward.flatten_state(episode, state)
                action = policy[flatState]
                state, r, done = time_step(env, steps, action)
                score += r
                if done:
                    break
            summedScore += score
            if score > high_score:
                print("sim %d new high score!" % sim)
                high_score = score
            score_list.append(score)
        average_score, std_dev = calculate_stats(summedScore, num_sims,
                score_list)
        print("policy: %s high score = %d, average score = %.2f" % (name, 
                high_score, average_score))
        summary[:,count] = [average_score, std_dev, high_score]
        if count > 0:
            header_string += ','
        header_string += p['name']

    np.savetxt('RANK_SUMMARY.csv', summary, delimiter=',', fmt='%.5f',
            header=header_string)


if __name__ == '__main__':
   sys.exit(run(sys.argv))
