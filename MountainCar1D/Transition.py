#!/bin/python3.5

import numpy as np
import gym
import math
from StateMapper import StateMap

class Transition(object):

    def __init__(self, env = None, n_bins = 10, steps = 10, episodes = 20, debug = False):
        if env == None:
            self.env = gym.make("MountainCar-v0")
        else:
            self.env = env
        if debug:
            self.debug = True
        else:
            self.debug = False
        self.state_map = StateMap(env, n_bins, steps, episodes)
        self.n_bins = n_bins
        self.n_episodes = episodes
        self.n_states = self.n_bins * self.n_bins + self.n_bins
        self.S_size = self.n_episodes * self.n_states + self.n_states
        self.matrix = np.zeros((3, self.S_size, self.S_size))
        self.build_transition_matrix()


    #state = tuple of (pos, v) action = 0, 1 or 2
    #returns s'
    def map_state_action_nstate(self, s, a):
        return self.state_map.map_state_action_nstate(s, a)


    #convert state from (pos, v) tuple to linear representaion
    def flatten_state(self, episode, s, posError = 0, vError = 0):
        return self.state_map.flatten_state(episode, s, posError, vError)


    #convert state from linear representation to (pos, v) tuple
    def expand_state(self, flatState):
        return self.state_map.expand_state(flatState)


    #state == tuple (pos, v)
    #action ==int elem {0, 1, 2}
    #nextState == tuple(floa, floatt)
    #P == transition matrix of shape [a][s][s]
    def set_error_probs(self, episode, state, action, nextState, P):
        nPos = nextState[0]
        nVel = nextState[1]
        for posError in (-1, 2, 1):
            for vError in (-1, 2, 1):
                index = np.clip(self.flatten_state(episode, nextState, posError,
                        vError), 0, self.S_size - 1)
                P[index] += 0.002
                if self.debug:
                    ep, _s = self.expand_state(index)
                    print("ep:" + str(ep) + " s:" + str(state) + " a:" \
                            + str(action) + " _s:" + str(_s) + " p=" \
                            + str(P[index]))


    #ep = step (episode)
    #s = state tuple (pos, v)
    #a = action
    #modifies m object to set probs of next state
    def set_transition_probs(self, ep, s, a, m):
        nextState = self.map_state_action_nstate(s, a)
        #mapped next state due to physics is most likely

        #set prob = 73%
        index = np.max([0, self.flatten_state(ep + 1, nextState)])
        index = np.min([index, len(m) - 1])
        m[index] += 0.982
        if self.debug:
            print("e:" + str(ep) + " s:" + str(s) + " a:" + str(a) + " _s:" \
                    + str(nextState) + " p=" + str(m[index]))
        #split remaining 27% equally in error in state(pos or velocity) due to
        #rounding
        self.set_error_probs(ep + 1, s, a, nextState, m)


    # T is list of {action} matricies of shape (n_state x n_states)
    def build_transition_matrix(self):
        for a in range (self.env.action_space.n):
            for row in range(self.S_size):
                ep, s = self.expand_state(row)
                self.set_transition_probs(ep, s, a, self.matrix[a][row])


    def __str__(self):
        return str(self.__class__.__name__) + ", state_space:" \
                + str(self.matrix.shape)
