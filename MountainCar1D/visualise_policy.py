#!/bin/python3.5

import glob, time
import os
import sys
import gym
import numpy as np
import scipy.io as sp
from StateMapper import StateMap as SM

env = gym.make("MountainCar-v0")
bins = 0
episodes = 0
steps = 0
policy = None
sim_out = None

try:
    param_file = open('MDP.params', 'r')
    for line in param_file.readlines():
        if "bins" in line:
            bins = int(line.replace("bins = ", ""))
        elif "episodes" in line:
            episodes = int(line.replace("episodes = ", ""))
        elif "steps" in line:
            steps = int(line.replace("steps = ", ""))
except Exception as e:
    print("faield to get param file!")
    print(e)
print("loaded params %d bins %d steps" % (bins, episodes))
try:
    print("loading policy from " + str(sys.argv[1]))
    policy = np.loadtxt(sys.argv[1])
    policy = policy.astype(int)
    print("file contains:" + str(policy))
    print("matrix.dimensions = " + str(policy.shape))

except Exception as e:
    print("failed to load policy!")
    print(e)


def time_step(env, steps, action):
    state = None
    score = 0
    done = False
    for step in range(steps):
        state, r, done, _ = env.step(action)
        env.render()
        time.sleep(0.2)
        score += r
        if done:
            break
    return state, score, done


state_map = SM(env, bins, steps, episodes)
for sim in range(1):
    state = env.reset()
    env.render()
    score = 0
    done = False
    input("Press Enter to continue...")
    for episode in range(episodes):
        flatState = state_map.flatten_state(episode, state)
        action = policy[flatState]
        state, r, done = time_step(env, steps, action)
        score += r
        if done:
            break
    print("Simulation %d: score = %d" % (sim, score))
    env.close()
