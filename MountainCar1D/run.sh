#!/bin/bash

python --version | grep "3.5"
if [ "$?" -ne "0" ]; then
    echo "Bad python detected need 3.5.x"
    exit 1
fi

python build_policy.py
python run_simulations.py

for POLICY in $(ls MC_*.policy); do STR=$( echo $POLICY | sed -e s/.policy//); echo $STR; mkdir -p $STR ; done
for POLICY in $(ls MC_*.policy); do echo $POLICY ; python simulate_policy.py $POLICY; done
