#!/bin/python3.5

import gym
import numpy as np
import math
from Reward import Reward

class Reward_straight_line(Reward):


    def __init__(self, env = "MountainCar-v0", n_bins = 50, steps = 10, 
            episodes = 20, debug = True):
        super(Reward_straight_line, self).__init__(env, n_bins, steps, 
                episodes, debug)
        self.build_reward_matrix()


    def calculate_reward(self, state):
        x_pos = state[0]
        y_pos = 0.45 * math.sin(3 * x_pos) + 0.55
        x_goal = 0.5
        y_goal = 0.45 * math.sin(3 * x_goal) + 0.55

        x_delta = x_pos - x_goal
        y_delta = y_pos - y_goal
        return - math.sqrt(math.pow(x_delta, 2) + math.pow(y_delta, 2))


    def set_reward(self, action, stateEpisode):
        ep, s = self.expand_state(stateEpisode)
        _s = self.map_state_action_nstate(s, action)
        _stateEpisode = np.clip(self.flatten_state(ep + 1, _s), 0,
                len(self.matrix[action][stateEpisode]) - 1)
        reward = self.calculate_reward(_s)
        if self.debug:
            print("ep:" + str(ep) + " s:" + str(s) + " _s:" + str(_s) + " r:" \
                    + str(reward))
        self.matrix[action][stateEpisode][_stateEpisode] = reward


    def suffix(self):
        return "sl"
