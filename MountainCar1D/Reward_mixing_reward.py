#!/bin/python3.5

import gym
import numpy as np
from Reward import Reward


class Reward_mixing_reward(Reward):


    def __init__(self, env = "MountainCar-v0", n_bins = 20, steps = 10,
            episodes = 20, reward_a = None, reward_b = None, prop = (0.5, 0.5), 
            debug = True):
        super(Reward_mixing_reward, self).__init__(env, n_bins, steps,
                episodes, debug)
        self.reward_a = reward_a
        self.prop_a = prop[0]
        self.reward_b = reward_b
        self.prop_b = prop[1]
        self.build_reward_matrix()


    def set_reward(self, action, stateEpisode):
        ep, s = self.expand_state(stateEpisode)
        _s = self.map_state_action_nstate(s, action)
        _stateEpisode = np.clip(self.flatten_state(ep + 1, _s), 0,
                len(self.matrix[action][stateEpisode]) - 1)
        self.matrix[action][stateEpisode][_stateEpisode] = reward = \
                self.reward_a.matrix[action][stateEpisode][_stateEpisode] * self.prop_a \
                + self.reward_b.matrix[action][stateEpisode][_stateEpisode] * self.prop_b
        if self.debug:
            print("ep:" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                    + " r:%.4f" % reward)


    def suffix(self):
        return "mi"
