#!/bin/python3.5

import gym
import numpy as np
import math
from Reward import Reward

class Reward_height_bonus(Reward):


    def __init__(self, env = "MountainCar-v0", n_bins = 50, steps = 10, 
            episodes = 20, debug = True):
        super(Reward_height_bonus, self).__init__(env, n_bins, steps, episodes, 
                debug)
        self.build_reward_matrix()


    def set_reward(self, action, stateEpisode):
        ep, s = self.expand_state(stateEpisode)
        _s = self.map_state_action_nstate(s, action)
        _stateEpisode = np.clip(self.flatten_state(ep + 1, _s), 0,
                len(self.matrix[action][stateEpisode]) - 1)
        self.matrix[action][stateEpisode][_stateEpisode] = reward = self.height(_s)

        if self.debug:
            print("ep:" + str(ep) + " s:" + str(s) + " _s:" + str(_s) + " r:" \
                    + str(reward))


    def suffix(self):
        return "hb"
