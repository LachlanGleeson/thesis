#!/bin/python3.5

import gym
import numpy as np
import math
from StateMapper import StateMap

#env = openai env
#n_bins = discretisation of state space
#steps = # of times action is repeated to calculate next state
#episodes = simulation length
class Reward(object):

    def __init__(self, env = None, n_bins = 15, steps = 10, episodes = 20, debug = True):
        self.debug = debug
        
        if isinstance(env, str):
            self.env = gym.make(env)
        else:
            self.env = env
        self.state_map = StateMap(env, n_bins, steps, episodes)
        self.n_bins = n_bins
        self.n_states = self.n_bins * self.n_bins + self.n_bins
        self.n_episodes = episodes
        self.S_size = self.n_episodes * self.n_states + self.n_states
        if (self.debug):
            print(type(self.n_states))
            print("n_bins =" + str(self.n_bins) + " n_states=" +
                    str(self.n_states) + " state space =" + str(self.S_size))
        self.matrix= np.zeros([3, self.S_size, self.S_size])


    # return height of car
    def height(self, s):
        return self.state_map.height(s)


    #return (float) force of gravity acting on the car at a given state
    def g_force(self, s):
        return self.state_map.g_force(s)


    #state = tuple of (pos, v) action = 0, 1 or 2
    #returns s'
    def map_state_action_nstate(self, s, a):
        return self.state_map.map_state_action_nstate(s, a)


    #convert state from (pos, v) tuple to linear representaion
    def flatten_state(self, episode, s, posError = 0, vError = 0):
        return self.state_map.flatten_state(episode, s, posError, vError)


    #convert state from linear representation to (pos, v) tuple
    def expand_state(self, flatState):
        return self.state_map.expand_state(flatState)


    def set_reward(self, action, state):
        raise NotYetImplementedException('You should overwrite this method!') 


    def build_reward_matrix(self):
        for a in range(self.env.action_space.n):
            for row in range(self.S_size):
                self.set_reward(a, row)


    def __str__(self):
        return str(self.__class__.__name__) + ", state_space:" \
                + str(self.matrix.shape)
