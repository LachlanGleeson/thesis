#!/bin/python3.5

import gym
import numpy as np
from Reward import Reward

class Reward_movement_cost(Reward):

    def __init__(self, env = "MountainCar-v0", n_bins = 50, steps = 10, 
            episodes = 20, debug = True):
        super(Reward_movement_cost, self).__init__(env, n_bins, steps, 
                episodes, debug)
        self.build_reward_matrix()


    def set_reward(self, action, stateEpisode):
        ep, s = self.expand_state(stateEpisode)
        _s = self.map_state_action_nstate(s, action)
        _stateEpisode = np.max([0, self.flatten_state(ep + 1, _s)])
        _stateEpisode = np.min([_stateEpisode, 
                len(self.matrix[action][stateEpisode]) - 1]) 
    
        if _s[0] >= 0.5:
            self.matrix[action][stateEpisode][_stateEpisode] = 0
            if self.debug:
                print("ep:" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:1") 
        else:
            self.matrix[action][stateEpisode][_stateEpisode] = -0.02
            if self.debug:
                print("ep:" + str(ep) + " s:" + str(s) + "_s:" + str(_s) \
                        + " r:-0.02")


    def suffix(self):
        return "mc"
