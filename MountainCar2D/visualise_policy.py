#!/bin/python3.5

import glob, time
import os
import sys
import gym
import numpy as np
from Reward.Reward import Reward3D as R
from StateMapper import StateMapper3D as SM
from myenv.my_mountain_car import MountainCar3D as MC

env = MC()
bins = 0
dt_bins = 0
episodes = 0
steps = 0
policy = None

try:
    param_file = open('MDP.params', 'r')
    for line in param_file.readlines():
        if "bins" in line:
            bins = int(line.replace("bins = ", ""))
        elif "steps" in line:
            steps = int(line.replace("steps = ", ""))
        elif "episodes" in line:
            episodes = int(line.replace("episodes = ", ""))
        elif "dt" in line:
            dt_bins = int(line.replace("dt = ", ""))
except Exception as e:
    print("faield to get param file!")
    print(e)

print("loaded params %d bins %d steps" % (bins, steps))
try:
    print("loading policy from " + str(sys.argv[1]))
    policy = np.loadtxt(sys.argv[1])
    policy = policy.astype(int)
    print("file contains:" + str(policy))
    print("matrix.dimensions = " + str(policy.shape))
except Exception as e:
    print("failed to load policy!")
    print(e)


def time_step(env, steps, policy, state_map):
    score = 0
    done = False
    for step in range(steps):
        epState = state_map.flatten_state(0, env.state)
        action = policy[epState]
        state, r, done, _ = env.step(action)
        env.render()
        time.sleep(0.2)
        score += r
        if done:
            break
    return score, done

state_map = SM(env, bins, dt_bins, steps, episodes)
for sim in range(1):
    state = env.reset()
    env.render()
    score = 0
    done = False
    input("Press Enter to continue...")
    for episode in range(episodes):
        r, done = time_step(env, steps, policy, state_map)
        score += r
        if done:
            break
    print("Sim score = %d" % score)
env.close()
