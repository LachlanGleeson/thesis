#!/bin/python3.5

import gym
import numpy as np
import math


class StateMapper3D(object):


    def __init__(self, env, xy_bins, dt_bins, steps, episodes):
        if env == None or xy_bins < 1 or dt_bins < 1 or steps < 1 \
                or episodes < 1:
            raise InvalidArgumentException("Invalid argument detected")
        self.env = env
        self.divisions = np.array([xy_bins, xy_bins, dt_bins, dt_bins])
        self.steps = steps
        self.episodes = episodes
        #intermediate state of (x, y) || (x', y')
        self.n_bins = xy_bins
        self.t_bins = dt_bins
        self.i_states = self.n_bins * self.n_bins + self.n_bins
        self.j_states = self.t_bins * self.i_states + self.i_states
        self.k_states = self.t_bins * self.j_states + self.j_states
        self.s_size = self.episodes * self.k_states + self.k_states


    #x = x pos
    #y = y_pos
    #return (gravity_x, gravity_y)
    def g_force(self, x, y):
        mag, phi = self.env._gravity(x, y)
        return mag * np.cos(phi), mag * np.sin(phi)


    #state = (x, y, x', y')
    #return height_z
    def height(self, state):
       return self.env._height(state[0], state[1])


    #state = (x, y, x', y')
    #action = 0 to 4
    # return (_x, _y, _x', _y')
    def state_action_nstate(self, state, action):
        x_action = y_action = 0
        if action == 1:
            x_action = 1
        elif action == 2:
            y_action = 1
        elif action == 3:
            x_action = -1
        elif action == 4:
            y_action = -1
 
        self.env.state = state
        for step in range(self.steps):
            self.env.step(action)
        return self.env.state


    #state = [x, y, x', y']
    #(x, y) = x * self.n_bins + y
    #(x', y') = x' * self.n_bins + y'
    # return (x, y) * self.i_states + (x', y')
    def flatten_state(self, episode, state, xE = 0, yE = 0, dxE = 0, dyE = 0):
        env_low = self.env.low
        env_high = self.env.high
        env_dx = (env_high - env_low) / self.divisions
        #print(str(env_dx))
        #print("x = " + str(np.around((state[0] - env_low[0])/env_dx[0], 10)))
        #print("y = " + str(np.around((state[1] - env_low[1])/env_dx[1], 10)))
        #print("dx = " + str(np.around((state[2] - env_low[2])/env_dx[2], 10)))
        #print("dy = " + str(np.around((state[3] - env_low[3])/env_dx[3], 10)))
        x = np.clip(int(np.around((state[0] - env_low[0])/env_dx[0], 6) + xE), 0,
                self.n_bins)
        y = np.clip(int(np.around((state[1] - env_low[1])/env_dx[1], 6) + yE), 0,
                self.n_bins)
        dx =  np.clip(int(np.around((state[2] - env_low[2])/env_dx[2], 6) +
                dxE), 0, self.n_bins)
        dy = np.clip(int(np.around((state[3] - env_low[3])/env_dx[3], 6) + 
                dyE), 0, self.n_bins)
        
        x_y = (x * self.n_bins) + y
        x_y_dx = (dx * self.i_states) + x_y
        x_y_dx_dy = (dy * self.j_states) + x_y_dx
        result = (episode * self.k_states) + x_y_dx_dy
        #print(str(x) + " " + str(y) + " " + str(dx) + " " + str(dy))
        #print(str(x_y) + " " + str(x_y_dx) + " " + str(x_y_dx_dy))
        return result


    #state = [x, y, x', y']
    def expand_state(self, s):
        episode = int(s / self.k_states)
        _state = s % self.k_states
        _dy = int(_state / self.j_states)
        if _dy > self.n_bins:
            _dx = self.n_bins
            _x = self.t_bins
            _y = self.t_bins
        else:
            _x_y_dx = _state % self.j_states
            _dx = int(_x_y_dx / self.i_states)
            _x_y = _x_y_dx % self.i_states
            _x = int(_x_y / self.n_bins)
            _y = int(_x_y % self.n_bins)

        env_low = self.env.low
        env_high = self.env.high
        env_dx = (env_high - env_low) / self.divisions
        x = np.clip(env_low[0] + (env_dx[0] * _x), env_low[0], env_high[0])
        y = np.clip(env_low[1] + (env_dx[1] * _y), env_low[1], env_high[1])
        dx = np.clip(env_low[2] + (env_dx[2] * _dx), env_low[2], env_high[2])
        dy = np.clip(env_low[3] + (env_dx[3] * _dy), env_low[3], env_high[3])
        return episode, (x, y, dx, dy)
