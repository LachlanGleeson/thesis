#!/bin/python3.5

import glob, time
import os
import sys
import numpy as np
from Reward.Reward import Reward3D as R
from StateMapper import StateMapper3D as SM
from myenv.my_mountain_car import MountainCar3D as MC


verbose = True
env = MC()
bins = 0
dt_bins = 0
episodes = 0
steps = 0

def load_policies():
    policies = []
    for fileName in glob.glob('*.policy'):
        print(str(fileName))
        matrix = np.loadtxt(fileName)
        matrix = matrix.astype(int)
        print("file " + str(fileName) + " contains:" + str(matrix))
        print("matrix.dimensions = " + str(matrix.shape))
        policy = {'name': str(fileName), 'policy': matrix}
        policies.append(policy)
    return policies


def calculate_stats(summedScores, numSims, score_list):
    average = summedScores / numSims
    scoreList = np.array(score_list)
    square_diff = (scoreList - average)**2
    stdev = np.sqrt(np.sum(square_diff)/len(scoreList))
    return average, stdev


def time_step(env, steps, action):
    state = None
    score = 0
    done = False
    for step in range(steps):
        state, r, done, _ = env.step(action)
        score += r
        if done:
            break
    return state, score, done


def run(argv):
    global bins, dt_bins, episodes, verbose, steps
    policies = load_policies()
    try:
        param_file = open('MDP.params', 'r')
        for line in param_file.readlines():
            if "bins" in line:
                bins = int(line.replace("bins = ", ""))
            elif "dt" in line:
                dt_bins = int(line.replace("dt = ", ""))
            elif "steps" in line:
                steps = int(line.replace("steps = ", ""))
            elif "episodes" in line:
                episodes = int(line.replace("episodes = ", ""))
    except Exception as e:
        print("faield to get param file!")
        print(e)

    print("n_bins: " + str(bins))
    print("dt_bins = " + str(dt_bins))
    print("n_steps: " + str(steps))
    print("n_episodes: " + str(episodes))
    state_map = SM(env, bins, dt_bins, steps, episodes)
    summary = np.zeros(shape=(3, len(policies)))
    header_string = ''
    for count, p in enumerate(policies):
        policy = p['policy']
        name = p['name']
        high_score = -(episodes * steps)
        sum_score = 0
        score_list = []
        num_sims = 1000
        for sim in range(num_sims):
            state = env.reset()
            score = 0
            done = False
            for episode in range(episodes):
                flatState = state_map.flatten_state(episode, state)
                action = policy[flatState]
                state, r, done = time_step(env, steps, action)
                score += r
                if done:
                    break
            sum_score += score
            score_list.append(score)
            if score > high_score:
                print("sim %d new high score!" % sim)
                high_score = score
        
        average_score, std_dev = calculate_stats(sum_score, num_sims, score_list)
        print("policy: %s high score = %d, average score = %d" % (name, 
                high_score, average_score))
        summary[:,count] = [average_score, std_dev, high_score]
        if count > 0:
            header_string += ','
        header_string += p['name']

    np.savetxt('RANK_SUMMARY.csv', summary, delimiter=',', fmt='%.5f',
            header=header_string)


if __name__ == '__main__':
   sys.exit(run(sys.argv))
