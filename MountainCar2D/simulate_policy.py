#!/bin/python3.5

import glob, time
import os
import sys
import numpy as np
import scipy.io as sp
from StateMapper import StateMapper3D as SM
from myenv.my_mountain_car import MountainCar3D as MC

env = MC()
bins = 0
dt_bins = 0
episodes = 0
steps = 0
policy = None
sim_out = None

try:
    param_file = open('MDP.params', 'r')
    for line in param_file.readlines():
        if "bins" in line:
            bins = int(line.replace("bins = ", ""))
        elif "episodes" in line:
            episodes = int(line.replace("episodes = ", ""))
        elif "steps" in line:
            steps = int(line.replace("steps = ", ""))
        elif "dt" in line:
            dt_bins = int(line.replace("dt = ", ""))
except Exception as e:
    print("faield to get param file!")
    print(e)
print("loaded params %d bins %d steps" % (bins, episodes))
try:
    print("loading policy from " + str(sys.argv[1]))
    policy = np.loadtxt(sys.argv[1])
    policy = policy.astype(int)
    print("file contains:" + str(policy))
    print("matrix.dimensions = " + str(policy.shape))

except Exception as e:
    print("failed to load policy!")
    print(e)


def time_step(env, steps, action):
    state = None
    score = 0
    done = False
    for step in range(steps):
        state, r, done, _ = env.step(action)
        score += r
        if done:
            break
    return state, score, done


state_map = SM(env, bins, dt_bins, steps, episodes)
for sim in range(100):
    state = env.reset()
    score = 0
    done = False
    sim_path = np.array([state[0], state[1], state[2], state[3], 0])
    for episode in range(episodes):
        flatState = state_map.flatten_state(episode, state)
        action = policy[flatState]
        state, r, done = time_step(env, steps, action)
        score += r
        sim_path = np.vstack([sim_path, [state[0], state[1], state[2], state[3], episode  + 1]])
        if done:
            break
    prefix = sys.argv[1].replace(".policy", "") 
    np.savetxt( prefix + "/" + prefix + "." + str(sim) + ".csv" , sim_path,
            delimiter=',', fmt='%.5f')
    print("Simulation %d: score = %d" % (sim, score))
