#!/bin/python3.5
"""
3D mountain car problem

@author: Lachlan Gleeson <lachlan.gleeson@uqconnect.edu.au>
"""
import gym
import numpy as np
import math
import gym.spaces as spaces
from gym.utils import seeding
import random

class MountainCar3D(gym.Env):
    
    def __init__(self):
       
        #max sim length
        self.max_sim = 200
        self.sim_len = 0
        #max displacement from origin in x or y direction
        self.max_pos = math.pi / 2
        #max velocity in either x or y direction
        self.max_velocity = 0.07
        self.goal = 0.5
        self.error = 0.1
        self.offset = 0.35
        self.low = np.array([-self.max_pos, -self.max_pos, -self.max_velocity,
        -self.max_velocity])
        self.high = np.array([self.max_pos, self.max_pos, self.max_velocity,
        self.max_velocity])

        self.viewer = None

        self.action_space = spaces.Discrete(5)
        self.observation_space = spaces.Box(low=self.low, high=self.high,
                dtype=np.float32)

        self.seed()
        self.reset()


    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]


    def step(self, action):
        assert self.action_space.contains(action), "%r (%s) invalid" % (action, type(action))
        x_action = y_action = 0
        if action == 1:
            x_action = 1
        elif action == 2:
            y_action = 1
        elif action == 3:
            x_action = -1
        elif action == 4:
            y_action = -1

        x, y, x_dot, y_dot = self.state
        g_mag, g_phi = self._gravity(x, y)
        x_dot += (x_action * 0.001) + (g_mag * np.cos(g_phi))
        y_dot += (y_action * 0.001) + (g_mag * np.sin(g_phi))

        x += x_dot
        y += y_dot
        self.state = self.__clip_state(x, y, x_dot, y_dot)
        reward, done = self.__sim_check(self.state[0], self.state[1])

        return np.array(self.state), reward, done, {}


    #return (reward, done)
    def __sim_check(self, x, y):
        mag, phi = self._polar(x, y)
        done = False
        if self.sim_len == self.max_sim:
            done = True
        self.sim_len += 1
        if self.goal - self.error < mag < self.goal + self.error:
            return 1, done
        else:
            return 0, done


    def __clip_state(self, xs, ys, x_dot, y_dot):
        mag, phi = self._polar(xs, ys)
        if mag > self.max_pos:
            return  self.max_pos * np.cos(phi), self.max_pos * np.sin(phi), 0.0, 0.0
        else:
            x_dot = np.clip(x_dot, -self.max_velocity, self.max_velocity)
            y_dot = np.clip(y_dot, -self.max_velocity, self.max_velocity)
            return xs, ys, x_dot, y_dot


    def _polar(self, xs, ys):
        mag = np.sqrt(xs**2 + ys**2)
        phi = np.arctan2(-ys, -xs) + np.pi
        return mag, phi


    def _gravity(self, xs, ys):
        mag = (-0.0025) * np.cos(5 * (xs**2 + ys**2)) / 5
        phi = np.arctan2(-ys, -xs) + np.pi
        return mag, phi


    def reset(self):
        polar_state = np.array([self.np_random.uniform(low=0.6, high=0.8),
        self.np_random.uniform(low=0, high=math.pi)])
        self.state = np.array([polar_state[0] * math.cos(polar_state[1]),
        polar_state[0] * math.sin(polar_state[1]), 0, 0])
        self.sim_len = 0
        return np.array(self.state)


    def _height(self, xs, ys = 0):
        if np.sqrt(xs**2 + ys**2) > self.max_pos:
            return self.offset
        else:
            return np.sin(5 * (xs**2 + ys**2)) / 5 + self.offset


    def _cartesian(self, rs, thetas):
        xs = rs * np.cos(thetas)
        ys = rs * np.sin(thetas)
        return xs, ys


    def render(self, mode='human'):
        screen_width = 1000
        screen_height = 400
        world_width = self.max_pos * 2
        tview_scale = screen_height/world_width
        carwidth = 20
        carheight = 20
        tview_offset = np.pi / 2 * tview_scale
        sview_offset = screen_width
        sview_wscale = (screen_height - screen_width)  / self.max_pos
        sview_hscale = tview_scale * 2.5

        if self.viewer == None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(screen_width, screen_height)
##Build top view of world
            r_outer_goal = 0.9
            r_inner_goal = 0.7
            thetas = np.linspace(0, 2 * np.pi, 360)
            l = b = -self.max_pos * tview_scale + tview_offset
            r = t = self.max_pos * tview_scale + tview_offset
            self.top_view = bounding_box = rendering.FilledPolygon([(l,b), 
                    (l,t), (r,t), (r,b)])
            arena = rendering.make_circle(self.max_pos * tview_scale, 200, True)
            arena.set_color(1, 1, 1) ## white
            outer_goal = rendering.make_circle((self.goal + self.error) * tview_scale, 
                    200, True)
            outer_goal.set_color(0, 1, 0) ## green
            inner_goal = rendering.make_circle((self.goal - self.error) * tview_scale, 
                    200, True)
            inner_goal.set_color(1, 1, 1) ## white
            arena.add_attr(rendering.Transform(translation=(tview_offset,
                    tview_offset)))
            outer_goal.add_attr(rendering.Transform(translation=(tview_offset,
                    tview_offset)))
            inner_goal.add_attr(rendering.Transform(translation=(tview_offset,
                    tview_offset)))
            #2D representaion of XY space with goal between inner circles
            self.viewer.add_geom(bounding_box)
            self.viewer.add_geom(arena)
            self.viewer.add_geom(outer_goal)
            self.viewer.add_geom(inner_goal)

            #Add car
            l,r,t,b = -carwidth/2, carwidth/2, carheight, 0
            car = rendering.FilledPolygon([(l,b), (l,t), (r,t), (r,b)])
            self.cartrans = rendering.Transform()
            self.viewer.add_geom(car)
            car.add_attr(self.cartrans)

##Build side view of world
            l = screen_height
            r = screen_width
            t = screen_height
            b = 0
            self.side_view = bounding_box = rendering.FilledPolygon([(l,b), (l,t), (r,t), (r,b)])
            bounding_box.set_color(1, 1, 1) ## white
            self.viewer.add_geom(bounding_box)
            xs = np.linspace(0, self.max_pos, 100)
            ys = np.array([self._height(x) for x in xs])
            
            xys = list(zip((xs + self.max_pos) * sview_wscale, 
                    ys * sview_hscale))
            self.track = rendering.make_polyline(xys)
            self.track.set_linewidth(4)
            self.track.add_attr(rendering.Transform(translation=(
                    screen_width + 1.5 * screen_height, 100)))
            self.viewer.add_geom(self.track)
            l,r,t,b = -carwidth/2, carwidth/2, carheight, 0
            side_car = rendering.FilledPolygon([(l,b), (l,t), (r,t), (r,b)])
            #clearance off track
            side_car.add_attr(rendering.Transform(translation=(0, 10)))
            self.sidecartrans = rendering.Transform()
            self.viewer.add_geom(side_car)
            side_car.add_attr(self.sidecartrans)

        #Translate/rotate for new car position
        x_pos = self.state[0]
        y_pos = self.state[1]
        self.cartrans.set_translation((x_pos * tview_scale) + tview_offset,
                (y_pos * tview_scale) + tview_offset)
        mag, phi = self._polar(x_pos, y_pos)
        self.cartrans.set_rotation(phi)
        height = self._height(-mag)
        self.sidecartrans.set_translation((mag * sview_wscale) +
               sview_offset, (height * sview_hscale) + 100)
        self.sidecartrans.set_rotation(-phi)
        return self.viewer.render(return_rgb_array = mode=='rgb_array')


    def close(self):
        if self.viewer: self.viewer.close()
