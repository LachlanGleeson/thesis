#!/bin/python3.5

import numpy as np
import gym
import math
from myenv.my_mountain_car import MountainCar3D
from StateMapper import StateMapper3D


class Transition3D(object):


    def __init__(self, env = None, n_bins = 5, dt_bins = 3, steps = 5, 
            episodes = 10, debug = False):
        if env == None:
            self.env = MountainCar3D()
        else:
            self.env = env

        if debug:
            self.debug = True
        else:
            self.debug = False
        self.state_map = StateMapper3D(env, n_bins, dt_bins, steps, episodes)
        self.n_bins = n_bins
        self.dt_bins = dt_bins
        self.episodes = episodes
        self.i_states = self.n_bins * self.n_bins + self.n_bins
        self.j_states = self.dt_bins * self.i_states + self.i_states
        self.k_states = self.dt_bins * self.j_states + self.j_states
        self.s_size = self.episodes * self.k_states + self.k_states
        self.matrix = np.zeros((env.action_space.n, self.s_size, self.s_size),
                dtype=np.float64)
        self.build_transition_matrix()


    #state = tuple of (pos, v) action = 0, 1 or 2
    #returns s'
    def map_state_action_nstate(self, s, a):
        return self.state_map.state_action_nstate(s, a)


    #convert state from (pos, v) tuple to linear representaion
    def flatten_state(self, episode, s, xE = 0, yE= 0, dxE = 0, dyE = 0):
        return self.state_map.flatten_state(episode, s, xE, yE, dxE, dyE)


    #convert state from linear representation to (pos, v) tuple
    def expand_state(self, flatState):
        return self.state_map.expand_state(flatState)


    def set_error_probs(self, ep, state, action, nState):
        s = self.flatten_state(ep, state)
        nextEp = ep + 1
        for xE in range(-1, 2, 1):
            for yE in range(-1, 2, 1):
                for dxE in range (-1, 2, 1):
                    for dyE in range (-1, 2, 1):
                        ns = np.clip(self.flatten_state(nextEp, nState, xE, yE, 
                                dxE, dyE), 0, self.s_size - 1)
                        self.matrix[action][s][ns] += 0.002


    def set_transition_prob(self, ep, state, action):
        n_state = self.map_state_action_nstate(state, action)
        nState = np.clip(self.flatten_state(ep + 1, n_state), 0, 
                self.s_size - 1)
        s = self.flatten_state(ep, state)

        self.matrix[action][s][nState] += 0.838
        if self.debug:
            print("ep:"+str(ep)+" s:" + str(state) + " a:" + str(action) + " _s:" \
                    + str(n_state) + " p=" + str(self.matrix[action][s][nState]))
        self.set_error_probs(ep, state, action, n_state)


    def build_transition_matrix(self):
        for action in range(self.env.action_space.n):
            for epState in range(self.s_size):
                episode, state = self.expand_state(epState)
                self.set_transition_prob(episode, state, action)
