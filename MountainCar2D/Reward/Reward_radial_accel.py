#!/bin/python3.5

import gym
import numpy as np
from Reward.Reward import Reward3D as Reward


#Reward states that move towards both x and y having positive acceleration 
#(reward will always move towards the largest acceleration
class Reward_radial_accel(Reward):


    def __init__(self, env = None, n_bins = 5, dt_bins = 3, steps = 5, 
            episodes = 10, debug = True):
        super(Reward_radial_accel, self).__init__(env, n_bins, dt_bins, steps, 
                episodes, debug)
        self.build_reward_matrix()


    def set_reward(self, action, epState):
        ep, s = self.expand_state(epState)
        _s = self.map_state_action_nstate(s, action)
        _epState = np.clip(self.flatten_state(ep + 1, _s), 0, self.s_size - 1)
        mag, phi = self.polar(s[0], s[1])
        _mag, _phi = self.polar(_s[0], _s[1])
        if (self.env.goal - self.env.error) < _mag < (self.env.goal 
                + self.env.error):
            self.matrix[action][epState][_epState] = 100
            if self.debug:
                print("ep" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:100")
        elif (_mag > self.env.goal + self.env.error) and (_mag < mag):
            self.matrix[action][epState][_epState] = 1
            if self.debug:
                print("ep" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:1")
        elif (_mag < self.env.goal - self.env.error) and (_mag > mag):
            self.matrix[action][epState][_epState] = 1
            if self.debug:
                print("ep" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:1") 
        else:
            self.matrix[action][epState][_epState] = -1
            if self.debug:
                print("ep:" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                        + " r:-1")


    def suffix(self):
        return "ra"
