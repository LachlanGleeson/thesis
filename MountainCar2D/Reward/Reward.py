#!/bin/python3.5

import gym
import numpy as np
import math
from myenv.my_mountain_car import MountainCar3D
from StateMapper import StateMapper3D as SM

class Reward3D(object):


    def __init__(self, env = None, n_bins = 8, dt_bins = 3, steps = 5, 
            episodes = 10, debug = True):
        self.debug = debug
        
        if isinstance(env, str):
            self.env = MountainCar3D()
        else:
            self.env = env
        self.state_map = SM(env, n_bins, dt_bins, steps, episodes)
        self.n_bins = n_bins
        self.dt_bins = dt_bins
        self.episodes = episodes
        self.i_states = self.n_bins * self.n_bins + self.n_bins
        self.j_states = self.dt_bins * self.i_states + self.i_states
        self.k_states = self.dt_bins * self.j_states + self.j_states
        self.s_size = self.episodes * self.k_states + self.k_states
        if (self.debug):
            print(type(self.s_size))
            print("n_bins =" + str(self.n_bins) + " n_states=" +
                    str(self.j_states) + " state space =" + str(self.s_size))
        self.matrix= np.zeros([self.env.action_space.n, self.s_size, 
                self.s_size], dtype=np.float16)


    # return height of car
    def height(self, s):
        return self.state_map.height(s)


    def polar(self, xs, ys):
        return self.env._polar(xs, ys)


    #return (float) force of gravity acting on the car at a given state
    def g_force(self, x, y):
        return self.state_map.g_force(x, y)


    #state = tuple of (pos, v) action = 0 to 5
    #returns s'
    def map_state_action_nstate(self, s, a):
        return self.state_map.state_action_nstate(s, a)


    #convert state from (pos, v) tuple to linear representaion
    def flatten_state(self, ep, s, xE = 0, yE= 0 , dxE = 0, dyE = 0):
        return self.state_map.flatten_state(ep, s, xE, yE, dxE, dyE)


    #convert state from linear representation to (pos, v) tuple
    def expand_state(self, flatState):
        return self.state_map.expand_state(flatState)


    def set_reward(self, action, epState):
        raise NotYetImplementedException('You should overwrite this method!') 


    def build_reward_matrix(self):
        for a in range(self.env.action_space.n):
            for row in range(self.s_size):
                self.set_reward(a, row)
