#!/bin/python3.5

import gym
import numpy as np
from Reward.Reward import Reward3D as Reward


#Reward states that move towards both x and y having positive acceleration 
#(reward will always move towards the largest acceleration
class Reward_height_bonus(Reward):


    def __init__(self, env = None, n_bins = 5, dt_bins = 3, steps = 5, 
            episodes = 10, debug = True):
        super(Reward_height_bonus, self).__init__(env, n_bins, dt_bins, steps, 
                episodes, debug)
        self.build_reward_matrix()


    def set_reward(self, action, epState):
        ep, s = self.expand_state(epState)
        _s = self.map_state_action_nstate(s, action)
        _epState = np.clip(self.flatten_state(ep + 1, _s), 0, self.s_size - 1)
        mag, phi = self.polar(_s[0], _s[1])
        self.matrix[action][epState][_epState] = reward = self.height(_s[0],
                _s[1])
        if self.debug:
            print("ep" + str(ep) + " s:" + str(s) + " _s:" + str(_s) \
                    + " r:" + str(reward))


    def suffix(self):
        return "hb"
