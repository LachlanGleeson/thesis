from myenv.my_mountain_car import MountainCar3D as MC
import numpy as np
env = MC()
bins = 7
dt_bins = 3
steps = 5
episodes = 10
debug = True
from Transition import Transition3D as T
transition = T(env, bins, dt_bins, steps, episodes, debug)
import numpy as np

valid = True
for action in range(env.action_space.n):
    for state in range(transition.s_size):
        prob = 0.0
        for nextState in range(transition.s_size):
            prob += transition.matrix[action][state][nextState]
        if prob != 1.0:
            print("prob = " + str(prob))
            raise Exception("invalide prob sum for action: " 
                    + "%d, state: %d" % (action, state))
