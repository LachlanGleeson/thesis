#!/bin/python3.5

import sys
import getopt
from myenv.my_mountain_car import MountainCar3D as MountainCar
from Transition import Transition3D as Transition
from Reward.Reward_true_reward import Reward_true_reward as Reward
from Reward.Reward_match_xy_accel import Reward_match_xy_accel as rMatchXY
from Reward.Reward_cent_accel import Reward_cent_accel as rCentAccel
from Reward.Reward_radial_accel import Reward_radial_accel as rRadialAccel
from Reward.Reward_straight_line import Reward_straight_line as rStraightLine
from Reward.Reward_penalise_outer import Reward_penalise_outer as rPenaliseOuter
from Reward.Reward_squeeze_function import Reward_squeeze_function as rSqueezeFunction
from Reward.Reward_boost_accel import Reward_boost_accel as rBoostAccel
import mdptoolbox
import numpy as np
import gym


d = 0.999
N = 1000000
bins = 9
dt_bins = 4
steps = 8
episodes = 25
eps=0.0000001
verbose = False
file_prefix = 'MC3D_'

def useage(argv):
    print("Python 3.5.4 script to run MDP toolbox on OpenAI gym classic")
    print("control environments\n\nusage:")
    print(argv[0] + " [--output {filename}] [--discout {d}] [--max-iter {i}] || --help")


def parse_args(argv):
    global verbose, d, N, bins, dt_bins, filename, eps
    try:
        opts, args = getopt.getopt(argv[1:], "vhb:d:N:p:e:", ["verbose", "help",
        "bins=", "discount=", "max-iter=", "prefix=", "epsilon="])
    except getopt.GetoptError:
        useage(argv)
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            useage(argv)
            sys.exit(0)
        elif o in ("-d", "--disount"):
            d = a
        elif o in ("-N", "--max-iter"):
            N = a
        elif o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-b", "--bins"):
            bins = a
        elif o in ("-p", "--prefix"):
            filename = a
        elif o in ("-e", "--epsilon"):
            eps = a


def solve_MDP(transition, reward):
    MDP_vi = mdptoolbox.mdp.ValueIteration(transition.matrix, reward.matrix,
            d, epsilon=eps, max_iter=N, initial_value=0, skip_check=True)
    print("MDP loaded sucessfully!")
    if verbose:
       MDP_vi.setVerbose()

    MDP_vi.run()
    print("MDP_vi complete!")
    if verbose:
        print("T:" + str(transition) + " R:" + str(reward))
        print(MDP_vi.policy)
        print("policy len = " + str(len(MDP_vi.policy)))
        print("solved in " + MDP_vi.iter + " iterations")
    np.savetxt( file_prefix + reward.suffix() + ".policy", MDP_vi.policy)


def run(argv):
    parse_args(argv)

    env = MountainCar()

    transition = Transition(env, bins, dt_bins, steps, episodes, verbose)
    reward = r_true_reward = Reward(env, bins, dt_bins, steps, episodes, verbose)
    a, s, _s = np.shape(reward.matrix)
    print("MDP space:\na = " + str(a) + " s = " + str(s) + " _s = " + str(_s))
    r_match_xy = rMatchXY(env, bins, dt_bins, steps, episodes, verbose)
    r_cent_accel = rCentAccel(env, bins, dt_bins, steps, episodes, verbose)
    r_radial_accel = rRadialAccel(env, bins, dt_bins, steps, episodes, verbose)
    r_straight_line = rStraightLine(env, bins, dt_bins, steps, episodes, verbose)
    r_penalise_outer = rPenaliseOuter(env, bins, dt_bins, steps, episodes, verbose)
    r_squeeze_function = rSqueezeFunction(env, bins, dt_bins, steps, episodes, verbose)
    r_boost_accel = rBoostAccel(env, bins, dt_bins, steps, episodes, verbose)
    reward_list = [r_true_reward, r_match_xy, r_cent_accel, r_radial_accel,
            r_straight_line, r_penalise_outer, r_squeeze_function,
            r_boost_accel]
    for r in reward_list:
        solve_MDP(transition, r)
        ##use reward policy here##
    print("Writing run parameters to MDP.params")
    try:
        file_object = open('MDP.params', 'w')
        file_object.write("bins = %d\n" % bins)
        file_object.write("dt = %d\n" % dt_bins)
        file_object.write("steps = %d\n" % steps)
        file_object.write("episodes = %d\n" % episodes)
        file_object.close()
    except Exception as e:
        print("file write failed!")
        print(e)
        


if __name__ == '__main__':
    run(sys.argv)
