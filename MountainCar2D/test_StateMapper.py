from myenv.my_mountain_car import MountainCar3D as MC
env = MC()
bins = 6
steps = 5
episodes = 10
debug = True
from StateMapper import StateMapper3D as SM
sm = SM(env, bins, steps, episodes)
for state in range(sm.s_size + 1):
    ep, s = sm.expand_state(state)
    test_state = sm.flatten_state(ep, s)
    if(test_state != state):
        raise Exception("state: %d; test state: %d" % (state, test_state))

