#!/bin/python3.5

import numpy as np
import gym
import math
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def surface(x, y):
    #return np.sin( 10.0 * (np.power(x, 2) + np.power(y, 2))) / 10.0
    if np.sqrt(x**2 + y**2) > math.pi/2:
        return 0
    else:
        return np.sin(5 * (x**2 + y**2)) / 5


def vector_field(x, y, z):
    mag = -0.25 * np.cos(5 * (x**2 + y**2)) / 5
    phi = np.arctan2(y, x)
    return (mag * np.cos(phi), mag * np.sin(phi), mag * 0)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = y = np.arange( -math.pi/2, math.pi/2, 0.05)
X, Y = np.meshgrid(x, y)
zs = np.array([surface(x,y) for x,y in zip(np.ravel(X), np.ravel(Y))])
Z = zs.reshape(X.shape)

x_dot, y_dot, z_dot = vector_field(X, Y, Z)
X_dot = x_dot.reshape(X.shape)
Y_dot = y_dot.reshape(X.shape)
Z_dot = z_dot.reshape(X.shape)
print(str(np.shape(X)))
print(str(np.shape(Y)))
print(str(np.shape(Z)))
print(str(np.shape(X_dot)))
print(str(np.shape(Y_dot)))
print(str(np.shape(Z_dot)))
ax.quiver(X, Y, Z, X_dot, Y_dot, Z_dot, color='k', length=0.5)
#ax.plot_surface(X, Y, Z)
ax.set_zbound(0, 0.5)
ax.set_xlabel('X position')
ax.set_ylabel('Y position')
ax.set_zlabel('Height')
#plt.savefig('test.png', dpi = 1200)
plt.show()
