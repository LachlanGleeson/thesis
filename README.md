# Thesis

Python impelmentation of MDP's to compare performace of hypothesised reward
functions against a known expert reward function.

Transition matrix implemented using environment pysics to denfie the most
likely state then distributing remaining % points as quantisation error when
defining the next state (state is quantized into default=50 bins for position
and velocity where each bin combination represents a unique state, error
intruduced when casting state from double to int)

REQUIREMENTS:
Python === 3.5
OpenaAI (only classic control required):
    https://github.com/openai/gym
PyMDP solver (must compile from source):
    https://github.com/sawcordwell/pymdptoolbox
